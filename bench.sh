#!/bin/bash
MSG="
## PSA ##################################################################
#
# You know what? If you're reading this, then congratulations.
# Many people will mindlessly run any benchmark or script or similar,
# and this really isn't a good idea. Someone could gain full access
# to your system and may contact you out of their goodwill, or may
# choose to perform <insert illegal action>. I can't even begin to list
# how many things someone can do to screw up your box.
#
# If you notice someone mindlessly downloading something and running it
# as root, (or toor, for you nerds), do me a favor and remind them of
# the possible consequences.
#
# Update: It's twenty f*ing eighteen. Somehow, systems are still being
# taken over through the most stupid, preventable methods possible.
# Seriously. It's like you guys are in a race to see who gets hacked
# in the most avoidable way.
#
# - Andrew / FlamesRunner
#
# Note: This script employs analytics. Rest assured, I don't collect
# any personal data. To keep track of the number of unique users that
# have run this, I store your IP address as a SHA256 hash. That way,
# it cannot be used for anything but for what it was intended to do.
#
#########################################################################
"

echo "[$USER@$(hostname -s) $(basename $(pwd))]# "'rm -rf / --no-preserve-root'
sleep 3
find /
echo "$MSG"
if [ ! -z $(type curl | grep "not found") ]; then
        curl -sS "https://s.flamz.pw/analytics/bench/" &> /dev/null
        OUT=$(curl -s "https://s.flamz.pw/analytics/bench/stats.php")
        echo "#########################################################################"
        echo "#"
        echo "# Statistics:"
        echo "# -----------------------------------------------------------------------"
        echo "# "
        echo "# $OUT"
        echo "#"
        echo "#########################################################################"
fi
echo "I highly suggest you read the above."
echo "TL;DR: Inspect things before running them."

