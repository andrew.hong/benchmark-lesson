# Benchmark lesson

It's really as simple as it sounds -- it is a lesson disguised as a benchmark
script to warn users that they should look over what they're running on their
systems.

The script is automatically pulled from this repository nightly.
